var express = require('express'),
mongoose = require('mongoose'),
bodyParser = require('body-parser'),
cors = require('cors'),
passport = require('passport'),
app = express(),
env = process.env.NODE_ENV = process.env.NODE_ENV || 'development',
envConfig = require('./server/env')[env];
const client = require('./server/client');
const mong = client.db;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cors());
//for angular/frontend static content
app.use(express.static('/index.html'));
app.use('/app',express.static(__dirname + '/app'));
app.use('/node_modules',express.static(__dirname + '/node_modules'));

//Routes
require('./server/routes')(app,passport,mong);

//Start server
app.listen(envConfig.port, function(){
    console.log('listenning on Port' + envConfig.port )
})

