
var mongoose = require('mongoose')
env = process.env.NODE_ENV = process.env.NODE_ENV || 'development',
envConfig = require('./env')[env];

mongoose.Promise = global.Promise;
mongoose.connect(envConfig.db).then(() =>  console.log('Mongo connection succesful'))
.catch((err) => console.error(err));
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:') );

module.exports = exports= {
    db
}



// //we can make seperate file and pass on envconfig in order to make index.js more thin
// mongoose.Promise = global.Promise;
// mongoose.connect(envConfig.db).then(() =>  console.log('Mongo connection succesful'))
// .catch((err) => console.error(err));
// var db = mongoose.connection;
// db.on('error', console.error.bind(console, 'MongoDB connection error:') );