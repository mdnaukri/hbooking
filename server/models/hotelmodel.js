var mongoose = require('mongoose');

var hotelSchema = new mongoose.Schema({

hotel_name: { type: String, required: '{PATH} is required!'},
      address: {type: String},
      pincode: {type: Number},
      ratings: {type: Number},
      reviews: {type: Number},
      price: {type: Number},
      amenities: {type: Array}
})
module.exports = mongoose.model('Hotel', hotelSchema);