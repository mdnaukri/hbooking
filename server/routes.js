var express = require('express'),
    path = require('path'),
    Hotel = require('./models/hotelmodel'),
    rootPath = path.normalize(__dirname + '/../'),
    User = require('./models/user'),
    mongoose = require('mongoose')
    apiRouter = express.Router();
const raccoon = require('raccoon');
const expressJwt = require('express-jwt');
const redisClient = require('./importrediscli');

const bluebird = require('bluebird');

module.exports = function (app, passport,mong ) {
    
    app.use(express.json());
    app.use(express.urlencoded());

    app.post('/dislikehotel',expressJwt({ secret: 'server secret' }),function(req, res){
        var userId = req.user.id;
        var hotelId = req.body.hotelId;
        raccoon.liked(userId, hotelId).then(() => {
            return raccoon.disliked(userId, hotelId);
        }).then(() => {
            return raccoon.recommendFor(userId, 10);
        }).then((recs) => {
            console.log('recs', recs);
            res.status(200).send(JSON.stringify(recs));
        }).catch((error)=>{
            console.log("raccon dislike Error : " + error)
        });
    });

    app.get('/adump', function(req,res){
        redisClient.flushall();
        const insertUserHotelLists = function(user, hotel){
              const userId = user._id.toString();
              const hotelId = hotel._id.toString();
              const rating = hotel.ratings;
            
              if (rating > 3){
                console.log(" Email "+user.email +"Here userid : " +userId +" here hotelid : " +hotelId + "Liked Hotel"+hotel.hotel_name);
                raccoon.liked(userId, hotelId);
              } else if (rating === '') {
                // do nothing
              } else {
                console.log(" Email "+user.email +" Here userid : " +userId +" here hotelid : " +hotelId + " DisLiked Hotel : "+hotel.hotel_name);
                raccoon.disliked(userId, hotelId);
              }        
          }; 
        
        Promise.all([
            User.find({}),
            Hotel.find({})
        ]).then(function(users,hotels){
            var operationCompleted = 0;
            function operation() {
                console.log("my increement"+ operationCompleted)
                ++operationCompleted;
                if (operationCompleted ==users[0].length) after_forloop();
            }

            for (let i = 0; i < users[0].length; i++) {

                //console.log("Hi we r there" + JSON.stringify(users[0][i]));
                for (let j = 0; j< users[1].length;j++){
                   // console.log("Hi we r there In hotels data" + JSON.stringify(users[1][j]));
                    insertUserHotelLists(users[0][i],users[1][j]);
                }
                operation();
            }
            
        }).catch((err)=>{
                console.log("Here is our error in dump",+ err)
        });
        function after_forloop() {
            res.status(200).send({ message: 'All Hotels Recomendations set' })
        }
    });




    app.post('/watchrecomend',expressJwt({ secret: 'server secret' }), function(req,res){
        let loginObject = {};
        
        var userId = req.user.id;
        console.log("koko" + userId);
              bluebird.Promise.all([
                    User.findById(userId),
                    User.find({}),
                    Hotel.find({})
                ]).spread(function(userObj,users,hotels){
                    //const user = userObj[0];
                    const userId = userObj._id;
                    const userName = userObj.email;
                    raccoon.stat.allWatchedFor(userId).then((allWatched)=>{
                        raccoon.stat.recommendFor(userId,5).then((recs)=>{
                                loginObject = {
                                    userId,
                                    users,
                                    hotels, 
                                    username: userName,
                                    alreadyWatched : allWatched,
                                    recommendations: recs	
                                };
                                var response = loginObject.recommendations;
                              
                                Hotel.find( { _id: { $in : response } } , function(error,hotels){
                                    if(error){ console.log("Here is Error : " + Error)}
                                    console.log("okay not save " +hotels );
                                    res.status(200).send(hotels);
                                });
                                
                                
                        })
                        
                    })
                });
    });

    app.get('/recommendedmostlikeset',expressJwt({ secret: 'server secret' }), function (req, res) {
        console.log("Here is my token name" + JSON.stringify(req.user));
        raccoon.mostLiked().then((results) => {
            console.log("My Raccoon Most Liked Hotel Data : " + results);
            res.status(200).send(JSON.stringify(results));
            return results;
        }).catch((error)=>{
                console.log("Error : "+ error)
        });
        
      
    });

    app.post('/raccon',expressJwt({ secret: 'server secret' }), function (req, res) {
        //var userId = req.body.id;
        var userId = req.user.id;
        var hotelId = req.body.hotelId;
        // var movie2Id = '5a1a73a70df6641a54d1a0a7';
        //var chrisId = '5a1aad1fd8c5bc2a4cba288e';
        raccoon.liked(userId, hotelId).then(() => {
            return raccoon.liked(userId, hotelId);
        }).then(() => {
            return raccoon.recommendFor(userId, 10);
        }).then((recs) => {
            console.log('recs', recs);
            // results will be an array of x ranked recommendations for chris 
            // in this case it would contain movie2 
            res.status(200).send(JSON.stringify(recs));
        }).catch((error)=>{
            console.log("raccon Error : " + error)
        });
    });

    passport.use(User.createStrategy());
    app.post('/login', passport.authenticate(
        'local', {
            session: false
        }), require('./serialize'), require('./generatetoken'), require('./respond'));
    //end token generation

    app.post('/register', function (req, res) {
        //passport-local-mongoose method to register
        User.register(new User({
            email: req.body.email,
            admin: req.body.admin || false
        }), req.body.password, function (err, user) {
            if (err) {
                console.log(err);
                res.status(201).json("User Already Exist");
            }
            else if (user) {
                res.status(201).json("Succesfully Created");
            }


        })
    });


    app.use('/api', expressJwt({ secret: 'server secret' }), apiRouter);
    require('./api/hotel')(apiRouter);

    app.post('/addhotels', function (req, res) {
        this.defaulthotels = req.body;
        console.log("hereeeeee" + req.body);
        var operationsCompleted = 0;
        function operation() {
            ++operationsCompleted;
            if (operationsCompleted == this.defaulthotels.length) after_forloop();
        }

        for (let defaulthotel of this.defaulthotels) {
            var hotel = new Hotel();
            hotel.hotel_name = defaulthotel.hotel_name;
            hotel.address = defaulthotel.address;
            hotel.pincode = defaulthotel.pincode;
            hotel.ratings = defaulthotel.ratings;
            hotel.reviews = defaulthotel.reviews;
            hotel.price = defaulthotel.price;
            hotel.amenities = defaulthotel.amenities;
            hotel.save(function (err, ninja) {
                if (err) console.log(err);
                operation();
            });
        }
        function after_forloop() {
            res.status(200).send({ message: 'All Hotels Successfully Added!' })
        }


    });


    app.get('/home', function (req, res) {
        res.sendFile(rootPath + '/index.html')
    });

    app.get('/*', function (req, res) {
        res.sendFile(rootPath + '/index.html')
    });
    app.use(function (req, res) {
        res.status(404);
        res.render('404');
        return;
    })


}