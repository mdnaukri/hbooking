Collection Link of Postman - https://www.getpostman.com/collections/1f982b07ebc200711544

1. Step First download zip file of redis and set environment variable
2. install mongodb and set env path and data folder in c drive C:data, C:data/log , C:data/db
3. Register few users by frontend you can up node index.js and go to url http://localhost:4000/Register 
4. You can login http://localhost:4000/home
5. Run postman post query http://localhost:4000/addhotels with body fill body with provided json file in app/data/hot_data.json
6. then import postman collection json or from above Link
7. then run postman get request- http://localhost:4000/adump  to dump some data 
8. Now Login and user will redirect to /list page 
9. Click on any register it will redirect to http://localhost:4000/hotel/5a1b0a69ffeef90c54f8806c particular id now
   this page actions are responsible for recommendations
10. In order to see data in recommendation link which is given in sidebar you need to click on register and confirm register 
    atleast 2-3 hotels so that it can run K-Nearest Neighbors Algorithm for Recommendations    
 