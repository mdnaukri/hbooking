module App.Services {
    export class ListService{   
        public getAllHotels;
        static $inject = ['$http'];
        constructor(private $http:ng.IHttpService){
            this.getAllHotels = $http.get('http://localhost:4000/api/gethotels').then(onSuccess, onError);
            function onSuccess(response): any {
                console.log(response.data);
                return response.data;
            };

            function onError(response) {
                alert("Error");
            };
        }
        }
    }

app.service('ListService', App.Services.ListService);