var App;
(function (App) {
    var Services;
    (function (Services) {
        var RecListService = /** @class */ (function () {
            function RecListService($http) {
                this.$http = $http;
                this.getAllRecHotels = $http.post('http://localhost:4000/watchrecomend', {}).then(onSuccess, onError);
                function onSuccess(response) {
                    console.log(response.data);
                    return response.data;
                }
                ;
                function onError(response) {
                    alert("Error");
                }
                ;
            }
            RecListService.$inject = ['$http'];
            return RecListService;
        }());
        Services.RecListService = RecListService;
    })(Services = App.Services || (App.Services = {}));
})(App || (App = {}));
app.service('RecListService', App.Services.RecListService);

//# sourceMappingURL=../source-maps/services/reclistservice.js.map
