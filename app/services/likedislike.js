var App;
(function (App) {
    var Services;
    (function (Services) {
        var LikeDislikeService = /** @class */ (function () {
            function LikeDislikeService($http) {
                this.$http = $http;
            }
            LikeDislikeService.prototype.LikeByID = function (hotelId) {
                this.$http.post('http://localhost:4000/raccon', { hotelId: hotelId }).then(function (response) {
                    console.log("Here is my like set from backend" + response);
                });
            };
            LikeDislikeService.prototype.DisLikeByID = function (hotelId) {
                this.$http.post('http://localhost:4000/dislikehotel', { hotelId: hotelId }).then(function (response) {
                    console.log("Here is my like set from backend" + response);
                });
            };
            LikeDislikeService.$inject = ['$http'];
            return LikeDislikeService;
        }());
        Services.LikeDislikeService = LikeDislikeService;
    })(Services = App.Services || (App.Services = {}));
})(App || (App = {}));
app.service('LikeDislikeService', App.Services.LikeDislikeService);
//ssh-add ~/.ssh/bit 

//# sourceMappingURL=../source-maps/services/likedislike.js.map
