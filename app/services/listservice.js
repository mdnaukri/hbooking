var App;
(function (App) {
    var Services;
    (function (Services) {
        var ListService = /** @class */ (function () {
            function ListService($http) {
                this.$http = $http;
                this.getAllHotels = $http.get('http://localhost:4000/api/gethotels').then(onSuccess, onError);
                function onSuccess(response) {
                    console.log(response.data);
                    return response.data;
                }
                ;
                function onError(response) {
                    alert("Error");
                }
                ;
            }
            ListService.$inject = ['$http'];
            return ListService;
        }());
        Services.ListService = ListService;
    })(Services = App.Services || (App.Services = {}));
})(App || (App = {}));
app.service('ListService', App.Services.ListService);

//# sourceMappingURL=../source-maps/services/listservice.js.map
