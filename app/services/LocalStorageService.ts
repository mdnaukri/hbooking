module App.Services {
    
    export class LocalStorageService {
        storage: any;
       
        static $inject = ['$localStorage'];
        constructor($localStorage) {
           this.storage = $localStorage;
            console.log(this.storage);
        }
    }
}
app.service('localStorageService', App.Services.LocalStorageService);