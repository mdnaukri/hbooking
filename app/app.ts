///<reference path="_all.d.ts"/>
var app = angular.module('app',['ngRoute','ngAnimate','ngStorage','ngMaterial','ngMdIcons']);

app.config(['$routeProvider' , '$locationProvider','$httpProvider', '$mdIconProvider','$mdThemingProvider',
($routeProvider, $locationProvider, $httpProvider, $mdIconProvider:angular.material.IIconProvider,$mdThemingProvider:angular.material.IThemingProvider)=>{
    $httpProvider.defaults.headers.common = { 'X-Requested-With': 'XMLHttpRequest' };
    $httpProvider.interceptors.push('authInterceptor');
    $mdIconProvider.icon('menu','app/content/svg/menu.svg',24);
   
    $locationProvider.html5Mode(true);
    $routeProvider
    .when('/home', {
        templateUrl: 'app/templates/home.html',
         controller: 'HotelController',
         controllerAs: 'hotelCntrl'
    })
    .when('/register', {
        templateUrl:'app/templates/register.html',
        controller : 'RegisterController',
        controllerAs: 'RegCntrl'
    })
    .when('/list',{
        templateUrl: 'app/templates/hotel/list.html',
        controller: 'ListController',
        controllerAs: 'listCntrl',
        resolve : {
            AllHotels : (ListService)=> { 
                return ListService.getAllHotels
             }
        }
    })
    .when('/hotel/:id',{
        templateUrl: 'app/templates/hotel/hotelpage.html',
        controller: 'HotelPageController',
        controllerAs: 'hotelPgCntrl',
        resolve : {
            AllHotels : (ListService)=> { 
                return ListService.getAllHotels
             }
        }
    })
    .when('/hotelrecom',{
        templateUrl: 'app/templates/hotel/rechotel.html',
        controller: 'RecController',
        controllerAs: 'recCntrl',
        resolve : {
            AllRecHotels : (RecListService)=> { 
                return RecListService.getAllRecHotels
             }
        }
    })
    
    .otherwise({
        redirectTo: '/home'
    });
    $mdThemingProvider.theme('default').primaryPalette('blue').accentPalette('red');
}
]); 