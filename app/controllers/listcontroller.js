var App;
(function (App) {
    var Controllers;
    (function (Controllers) {
        var ListController = /** @class */ (function () {
            function ListController($location, ListService, AllHotels) {
                this.$location = $location;
                this.ListService = ListService;
                this.AllHotels = AllHotels;
                this.hotelList = AllHotels;
            }
            ListController.prototype.goToRegister = function (hotel_name) {
                console.log('Here it is', hotel_name);
                this.$location.url('/hotel');
            };
            ListController.$inject = ['$location', 'ListService', 'AllHotels'];
            return ListController;
        }());
        Controllers.ListController = ListController;
    })(Controllers = App.Controllers || (App.Controllers = {}));
})(App || (App = {}));
app.controller('ListController', App.Controllers.ListController);

//# sourceMappingURL=../source-maps/controllers/listcontroller.js.map
