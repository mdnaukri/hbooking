module App.Controllers{
    import LoginModel = App.Models.LoginModel;
    interface ILoginBindings {
        user: LoginModel;
        lgnmsg:any;
    }

    interface ILoginControllerScope{
        lgnCntrl: HotelController;
    }

    interface ILoginController extends ILoginBindings {
        login(user: LoginModel): void;
    }
    export class HotelController implements ILoginController{
           

        public message: string;
        static $inject =['$mdSidenav','LoginService','$location'];

        toggleSidenav(): void{
            this.$mdSidenav('left').toggle();
        }


        public user: LoginModel;
        public lgnmsg: any;
        login(user: LoginModel) {
            var __this = this;
            this.user.email = user.email;
            this.user.password = user.password;
            this.LoginService.loginUserService(user)
                .then((response: any) => {
                    if(Error){
                        __this.lgnmsg = "Username or Password is Wrong!";
                    }
                    if(response){
                        __this.lgnmsg = response.data;
                        this.$location.url('/list');
                    }
                })
        }

        constructor(private $mdSidenav:angular.material.ISidenavService,private LoginService: App.Services.LoginService,private $location){
            this.message = "Welcome";
        }
    }
}

app.controller('HotelController', App.Controllers.HotelController);


