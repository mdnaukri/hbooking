module App.Controllers {
    export class ListController {
        public hotelList;
        
        goToRegister(hotel_name){
            console.log('Here it is', hotel_name);
            this.$location.url('/hotel');
        }
    
        
        static $inject = ['$location','ListService','AllHotels'];
        constructor(private $location,private ListService: App.Services.ListService, private AllHotels) {
               this.hotelList = AllHotels;

        }
    }

}


app.controller('ListController', App.Controllers.ListController);