var App;
(function (App) {
    var Controllers;
    (function (Controllers) {
        var HotelController = /** @class */ (function () {
            function HotelController($mdSidenav, LoginService, $location) {
                this.$mdSidenav = $mdSidenav;
                this.LoginService = LoginService;
                this.$location = $location;
                this.message = "Welcome";
            }
            HotelController.prototype.toggleSidenav = function () {
                this.$mdSidenav('left').toggle();
            };
            HotelController.prototype.login = function (user) {
                var _this = this;
                var __this = this;
                this.user.email = user.email;
                this.user.password = user.password;
                this.LoginService.loginUserService(user)
                    .then(function (response) {
                    if (Error) {
                        __this.lgnmsg = "Username or Password is Wrong!";
                    }
                    if (response) {
                        __this.lgnmsg = response.data;
                        _this.$location.url('/list');
                    }
                });
            };
            HotelController.$inject = ['$mdSidenav', 'LoginService', '$location'];
            return HotelController;
        }());
        Controllers.HotelController = HotelController;
    })(Controllers = App.Controllers || (App.Controllers = {}));
})(App || (App = {}));
app.controller('HotelController', App.Controllers.HotelController);

//# sourceMappingURL=../source-maps/controllers/hotelcontroller.js.map
