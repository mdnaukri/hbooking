var App;
(function (App) {
    var Controllers;
    (function (Controllers) {
        var HotelPageController = /** @class */ (function () {
            function HotelPageController($location, $routeParams, ListService, AllHotels, LikeDislikeService) {
                this.$location = $location;
                this.$routeParams = $routeParams;
                this.ListService = ListService;
                this.AllHotels = AllHotels;
                this.LikeDislikeService = LikeDislikeService;
                this.hotelList = AllHotels;
                this.id = $routeParams.id;
                console.log("My Page Id" + this.id);
                var _this = this;
                this.selectedhotel = this.hotelList.filter(function (hlist) {
                    if (hlist._id === _this.id) {
                        console.log("hotel" + hlist.hotel_name);
                        return hlist;
                    }
                })[0];
                console.log("hi" + this.selectedhotel);
            }
            HotelPageController.prototype.goToLike = function (hotelInfo) {
                console.log("Here is my like id" + this.id);
                this.LikeDislikeService.LikeByID(this.id);
                this.$location.url('/list');
            };
            HotelPageController.prototype.goToDisLike = function (hotelpg) {
                console.log("Here is my dislike id" + this.id);
                this.LikeDislikeService.DisLikeByID(this.id);
                this.$location.url('/hotel/' + hotelpg._id);
            };
            HotelPageController.$inject = ['$location', '$routeParams', 'ListService', 'AllHotels', 'LikeDislikeService'];
            return HotelPageController;
        }());
        Controllers.HotelPageController = HotelPageController;
    })(Controllers = App.Controllers || (App.Controllers = {}));
})(App || (App = {}));
app.controller('HotelPageController', App.Controllers.HotelPageController);

//# sourceMappingURL=../source-maps/controllers/hotelpagecontroller.js.map
