module App.Controllers {
    export class RecController {
        
        public hotelRecList;
        static $inject = ['$location','RecListService','AllRecHotels'];
        constructor(private $location,private RecListService: App.Services.ListService, private AllRecHotels) {
               this.hotelRecList = AllRecHotels;

        }
    }
    
    }
    
    
    app.controller('RecController', App.Controllers.RecController);