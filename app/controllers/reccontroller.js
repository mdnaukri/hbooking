var App;
(function (App) {
    var Controllers;
    (function (Controllers) {
        var RecController = /** @class */ (function () {
            function RecController($location, RecListService, AllRecHotels) {
                this.$location = $location;
                this.RecListService = RecListService;
                this.AllRecHotels = AllRecHotels;
                this.hotelRecList = AllRecHotels;
            }
            RecController.$inject = ['$location', 'RecListService', 'AllRecHotels'];
            return RecController;
        }());
        Controllers.RecController = RecController;
    })(Controllers = App.Controllers || (App.Controllers = {}));
})(App || (App = {}));
app.controller('RecController', App.Controllers.RecController);

//# sourceMappingURL=../source-maps/controllers/reccontroller.js.map
